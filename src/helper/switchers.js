class Switchers {
  actual = false;
  sitchOn = "";

  constructor(dato) {
    this.dato = dato;
  }

  activateBTN() {
    return {
      justifyContent: "flex-end",
    };
  }

  boxesSelector(params, selected) {
    params.map((box) => {
      document.getElementsByClassName(box)[0].classList.contains("active") &&
        document.getElementsByClassName(box)[0].classList.remove("active");
    });
    return selected;
  }

  activatedSelected(selected) {
    if (selected && document.getElementsByClassName(selected)[0])
      document.getElementsByClassName(selected)[0].classList.add("active");
  }
}

export default new Switchers();
