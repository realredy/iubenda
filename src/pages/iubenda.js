import React, { useState } from "react";
import logoIubenda from "../images/logo_1.png";
import Aceptances from "../sections/aceptances";
import Cookiebanner from "../sections/Cookiebanner";
import Domainporcent from "../sections/domainporcent";
import Buttonstyle from "../sections/buttonstyles";
import Switchers from "../helper/switchers";
import Foother from "../sections/Foother";

import "./iubenda.scss";
function Iubenda() {
  const [activeButton, setactiveButton] = useState(true);
  const [buttonstyles, setButtonstyles] = useState(true);

  return (
    <>
      <div className="Iubenda">
        <div className="Iubenda__wrapper">
          <div className="Iubenda__wrapper-header">
            <img src={logoIubenda} alt="iubenda" />
          </div>
          <main className="Iubenda__wrapper-main">
            <Domainporcent />
            {/* first module*/}
            <Aceptances />
            {/* second module*/}
            <div className="Iubenda__wrapper-main-cookieBannerButton">
              <div className="Iubenda__wrapper-main-cookieBannerButton-wrapper">
                <div
                  style={!activeButton ? Switchers.activateBTN() : {}}
                  className="Iubenda__wrapper-main-cookieBannerButton-wrapper-button"
                  onClick={() => {
                    setactiveButton(!activeButton);
                  }}
                >
                  <span></span>
                </div>
                <div className="Iubenda__wrapper-main-cookieBannerButton-wrapper-text">
                  <h2>Add a cookie banner</h2>
                </div>
              </div>
              {!activeButton && <Cookiebanner />}
              <div className="Iubenda__wrapper-main-cookieBannerButton-wrapper">
                <div
                  style={!buttonstyles ? Switchers.activateBTN() : {}}
                  className="Iubenda__wrapper-main-cookieBannerButton-wrapper-button"
                  onClick={() => {
                    setButtonstyles(!buttonstyles);
                  }}
                >
                  <span></span>
                </div>
                <div className="Iubenda__wrapper-main-cookieBannerButton-wrapper-text">
                  <h2>Add the privacy policy button</h2>
                </div>
              </div>
              {!buttonstyles && <Buttonstyle />}
            </div>
            <Foother />
          </main>
          <div className="Iubenda__wrapper-main-cookieBannerButton-wrapper-closeSession">
            <b>Documentation</b> <b>Contact support</b>
          </div>
        </div>
      </div>
    </>
  );
}

export default Iubenda;
