import React from "react";

import interRed from "../images/interRed.svg";

function Infomodule() {
  return (
    <div className="cookieBanner__wrapperOthers-buttons-autom-info">
      <img src={interRed} alt="admig_r" />
      <p>
        You may should activate this feature if you show ads on your website
      </p>
    </div>
  );
}

export default Infomodule;
