import React from "react";
import "./footer.scss";

function Foother() {
  return (
    <div className="Foother">
      <div className="Foother_wrapper">
        <span>Reset setting</span>
        <button>Integrate</button>
      </div>
    </div>
  );
}

export default Foother;
