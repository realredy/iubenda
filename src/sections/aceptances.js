import React from "react";
import CheckoutGreen from "../images/icon_checkmark-green.svg";
import "./aceptances.scss";
function Aceptances() {
  return (
    <>
      <section className="Iubenda__wrapper-aceptances">
        <div className="Iubenda__wrapper-aceptances-left">
          <img src={CheckoutGreen} alt="" />
        </div>
        <div className="Iubenda__wrapper-aceptances-center">
          <p>
            Your website has been created and your legal documents have
            generated. Setup your cookie banner and privacy policy button to
            complemte the integration.
          </p>
        </div>
        <div className="Iubenda__wrapper-aceptances-right">
          <span>+</span>
        </div>
      </section>
    </>
  );
}

export default Aceptances;
