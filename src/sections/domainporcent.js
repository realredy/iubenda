import React from "react";
import linealPc from "../images/pc.svg";
function Domainporcent() {
  return (
    <>
      <section className="Iubenda__wrapper-domainPorcent">
        <div className="Iubenda__wrapper-domainPorcent-left">
          <div className="Iubenda__wrapper-domainPorcent-left-wrapper">
            <div className="Iubenda__wrapper-domainPorcent-left-wrapper-pc">
              <img src={linealPc} alt="lineal pc" />
            </div>
            <div className="Iubenda__wrapper-domainPorcent-left-wrapper-info">
              <b>www.domain.com</b>
              <p>Account & Billing info</p>
            </div>
          </div>
        </div>
        <div className="Iubenda__wrapper-domainPorcent-right">
          <div className="Iubenda__wrapper-domainPorcent-right-wrapper">
            <b className="Iubenda__wrapper-domainPorcent-right-wrapper-title">
              Your current compliance rating
            </b>
            <div className="Iubenda__wrapper-domainPorcent-right-wrapper-portcentBox">
              <span>
                <p>23%</p>
              </span>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default Domainporcent;
