import React, { useState, useRef } from "react";
import fakebannerDark from "../images/fake_banner_dark.png";
import fakebannerLight from "../images/fake_banner_light.png";
import downArrow from "../images/arrow.svg";
import ueFlagGreen from "../images/icon_ue_flag-green.svg";
import iconWorld from "../images/icon_world.svg";
import simpleCheckMark from "../images/simpleCheckmark.svg";
import Switchers from "../helper/switchers";
import Infomodule from "../sections/infomodule";
import "./Cookiebanner.scss";

function Cookiebanner() {
  const [activeTheme, setActiveTheme] = useState("dark");
  const [activeLegislation, setActiveLegislation] = useState("GPRD_Only");
  const [consent, setConsent] = useState("GPRD_Only");

  const [activeinfoOne, setActiveinfoOne] = useState(false);
  const [activeinfoTwo, setActiveinfoTwo] = useState(false);
  const [activeinfoThree, setActiveinfoThree] = useState(false);

  // banners Buttons
  const [aceptButton, setAceptButton] = useState(true);
  const [rejectButton, setRejectButton] = useState(true);

  //Other Options
  const [iab, setIab] = useState(true);
  const [google, setGoogle] = useState(true);
  const [automaticaly, setAutomaticaly] = useState(true);

  const themes = ["dark", "light", "custom"];
  const legistaions = ["GPRD_Only", "CCPA_Only", "Both"];
  const consents = ["EU_Only", "World_Wide"];

  const selectTheme = (selected) => {
    setActiveTheme(Switchers.boxesSelector(themes, selected));
  };

  const setactLegislation = (selected) => {
    setActiveLegislation(Switchers.boxesSelector(legistaions, selected));
  };

  const setactConsent = (selected) => {
    setConsent(Switchers.boxesSelector(consents, selected));
  };

  if (activeTheme) Switchers.activatedSelected(activeTheme);

  if (activeLegislation) Switchers.activatedSelected(activeLegislation);

  if (consent) Switchers.activatedSelected(consent);

  return (
    <>
      <div className="cookieBanner">
        <div className="cookieBanner__wrapperTop">
          <div className="cookieBanner__wrapperTop-left">
            <span>Position</span>
            <div className="cookieBanner__wrapperTop-left-box">
              <img src={downArrow} alt="" />
            </div>
          </div>
          <div className="cookieBanner__wrapperTop-right">
            <span>Theme</span>
            <div className="cookieBanner__wrapperTop-right-wrapper">
              <div
                onClick={() => {
                  selectTheme("dark");
                }}
                className="cookieBanner__wrapperTop-right-wrapper-inside"
              >
                <div className="cookieBanner__wrapperTop-right-wrapper-inside-boxes dark active">
                  <img src={fakebannerDark} alt="" />
                </div>
                <span>Dark</span>
              </div>
              <div
                onClick={() => {
                  selectTheme("light");
                }}
                className="cookieBanner__wrapperTop-right-wrapper-inside"
              >
                <div className="cookieBanner__wrapperTop-right-wrapper-inside-boxes light">
                  <img src={fakebannerLight} alt="" />
                </div>
                <span>Light</span>
              </div>
              <div
                onClick={() => {
                  selectTheme("custom");
                }}
                className="cookieBanner__wrapperTop-right-wrapper-inside"
              >
                <div className="cookieBanner__wrapperTop-right-wrapper-inside-boxes custom">
                  <span>+</span>
                </div>
                <span>Custom Style</span>
              </div>
            </div>
          </div>
        </div>
        <div className="cookieBanner__wrapperMid">
          <span>Legislation</span>
          <div className="cookieBanner__wrapperMid-wrapper">
            <div
              onClick={() => {
                setactLegislation("GPRD_Only");
              }}
              className="cookieBanner__wrapperMid-wrapper-boxes GPRD_Only active"
            >
              GPRD Only
            </div>
            <div
              onClick={() => {
                setactLegislation("CCPA_Only");
              }}
              className="cookieBanner__wrapperMid-wrapper-boxes CCPA_Only"
            >
              CCPA Only
            </div>
            <div
              onClick={() => {
                setactLegislation("Both");
              }}
              className="cookieBanner__wrapperMid-wrapper-boxes Both"
            >
              Both
            </div>
          </div>
        </div>

        <div className="cookieBanner__wrapperEnd">
          <span>Require consent from</span>
          <div className="cookieBanner__wrapperEnd-wrapper">
            <div
              onClick={() => {
                setactConsent("EU_Only");
              }}
              className="cookieBanner__wrapperEnd-wrapper-boxes EU_Only active"
            >
              <img src={ueFlagGreen} alt="" />
              <p>EU Only</p>
            </div>
            <div
              onClick={() => {
                setactConsent("World_Wide");
              }}
              className="cookieBanner__wrapperEnd-wrapper-boxes World_Wide"
            >
              <img src={iconWorld} alt="" />
              <p>World Wide</p>
            </div>
          </div>
        </div>

        <div className="cookieBanner__wrapperSelectors">
          <span>Banner Buttons</span>
          <ul className="cookieBanner__wrapperSelectors-buttons">
            <li className="cookieBanner__wrapperSelectors-buttons-explicit">
              <span onClick={() => setAceptButton(!aceptButton)}>
                {aceptButton && <img src={simpleCheckMark} alt="" />}
              </span>
              <p>Explicit Accept and Customize buttons</p>
            </li>
            <li className="cookieBanner__wrapperSelectors-buttons-explicitReject">
              <span onClick={() => setRejectButton(!rejectButton)}>
                {rejectButton && <img src={simpleCheckMark} alt="" />}
              </span>
              <p>Explicit Reject buttons</p>
            </li>
          </ul>
        </div>

        <div className="cookieBanner__wrapperOthers">
          <span>Other Options</span>
          <ul className="cookieBanner__wrapperOthers-buttons">
            <li className="cookieBanner__wrapperOthers-buttons-IAB">
              <div className="cookieBanner__wrapperOthers-buttons-IAB-wrapper">
                <span onClick={() => setIab(!iab)}>
                  {iab && <img src={simpleCheckMark} alt="" />}
                </span>
                <p>Enable IAB Transparency and Consent Framework - Lean More</p>
                <b onClick={() => setActiveinfoOne(!activeinfoOne)}>?</b>
              </div>
              {activeinfoOne && <Infomodule />}
            </li>
            <li className="cookieBanner__wrapperOthers-buttons-Google">
              <div className="cookieBanner__wrapperOthers-buttons-Google-wrapper">
                <span onClick={() => setGoogle(!google)}>
                  {google && <img src={simpleCheckMark} alt="" />}
                </span>
                <p>Enable Google AMP support</p>
                <b onClick={() => setActiveinfoTwo(!activeinfoTwo)}>?</b>
              </div>
              {activeinfoTwo && <Infomodule />}
            </li>
            <li className="cookieBanner__wrapperOthers-buttons-autom">
              <div className="cookieBanner__wrapperOthers-buttons-autom-wrapper">
                <span onClick={() => setAutomaticaly(!automaticaly)}>
                  {automaticaly && <img src={simpleCheckMark} alt="" />}
                </span>
                <p>Automaticaly block script detected by the plugin</p>
                <b onClick={() => setActiveinfoThree(!activeinfoThree)}>?</b>
              </div>
              {activeinfoThree && <Infomodule />}
            </li>
          </ul>
        </div>
      </div>
    </>
  );
}

export default Cookiebanner;
