import React, { useState, useRef } from "react";
import fakebannerDark from "../images/fake_button_dark.png";
import fakebannerLight from "../images/fake_button_light.png";
import "./buttonstyle.scss";

function Buttonstyle() {
  return (
    <div className="buttonStyle">
      <div className="buttonStyle__wrapperTop">
        <span>Button Style</span>
        <div className="buttonStyle__wrapperTop-wrapper">
          <div className="buttonStyle__wrapperTop-wrapper-inside">
            <div className="buttonStyle__wrapperTop-wrapper-inside-boxes light active">
              <img src={fakebannerDark} alt="" />
            </div>
            <span>Light</span>
          </div>
          <div className="buttonStyle__wrapperTop-wrapper-inside">
            <div className="buttonStyle__wrapperTop-wrapper-inside-boxes dark">
              <img src={fakebannerLight} alt="" />
            </div>
            <span>Dark</span>
          </div>
        </div>
      </div>
      <div className="buttonStyle__wrapperPosition">
        <span>Button Position</span>
        <div className="buttonStyle__wrapperPosition-wrapper">
          <div className="buttonStyle__wrapperPosition-wrapper-left">
            <div className="buttonStyle__wrapperPosition-wrapper-left-span">
              <span className="buttonStyle__wrapperPosition-wrapper-left-span-simplepoint"></span>
            </div>
            <p>Put automaticaly button on footer</p>
          </div>
          <div className="buttonStyle__wrapperPosition-wrapper-right">
            <div className="buttonStyle__wrapperPosition-wrapper-right-span">
              <span className="buttonStyle__wrapperPosition-wrapper-right-span-simplepoint"></span>
            </div>
            <p>Integrate button manually</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Buttonstyle;
